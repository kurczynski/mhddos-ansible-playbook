# mhddos-ansible-playbook

Ansible playbook for the [IT Army of Ukraine](https://itarmy.com.ua/?lang=en) project
[MHDDOS](https://itarmy.com.ua/instruction/?lang=en#linux/#linux_mhddos/#linux_docker).
